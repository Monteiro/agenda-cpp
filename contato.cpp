//
//  contato.cpp
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include "contato.h"
#include <string>

Contato::Contato()
{
    setWpp("...");
    setEndereco("...");
}

Contato::Contato(string wpp, string endereco)
{
    this->wpp = wpp;
    this->endereco = endereco;
}

void Contato::setWpp(string wpp)
{
    this->wpp = wpp;
}

string Contato:: getWpp()
{
    return wpp;
}

void Contato::setEndereco(string endereco)
{
    this->endereco = endereco;
}

string Contato:: getEndereco()
{
    return endereco;
}
