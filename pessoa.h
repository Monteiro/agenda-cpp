//
//  pessoa.h
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#ifndef __agenda_cpp__pessoa__
#define __agenda_cpp__pessoa__

#include <iostream>
#include <string>

using namespace std;

class Pessoa
{
private:
    string nome;
    string idade;
    string telefone;
public:
    Pessoa();
    Pessoa(string nome, string idade, string telefone);
    string getNome();
    void setNome(string nome);
    string getIdade();
    void setIdade(string idade);
    string getTelefone();
    void setTelefone(string telefone);
};

#endif /* defined(__agenda_cpp__pessoa__) */
