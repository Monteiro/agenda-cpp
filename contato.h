//
//  contato.h
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#ifndef __agenda_cpp__contato__
#define __agenda_cpp__contato__

#include <string>
#include "pessoa.h"

using namespace std;

class Contato : public Pessoa
{
private:
    string wpp;
    string endereco;
public:
    Contato();
    Contato(string wpp, string endereco);
    string getWpp();
    void setWpp(string wpp);
    string getEndereco();
    void setEndereco(string endereco);
    
};



#endif /* defined(__agenda_cpp__contato__) */
