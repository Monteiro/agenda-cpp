//
//  amigo.cpp
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include "amigo.h"
#include <string>

using namespace std;

Amigo::Amigo()
{
    setApelido("...");
    setEmail("...");
}

Amigo::Amigo(string apelido, string email)
{
    this->apelido = apelido;
    this->email = email;
}

void Amigo::setApelido(string apelido)
{
    this->apelido = apelido;
}

string Amigo::getApelido()
{
    return apelido;
}

void Amigo::setEmail(string email)
{
    this->email = email;
}

string Amigo::getEmail()
{
    return email;
}








