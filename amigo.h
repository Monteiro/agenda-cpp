//
//  amigo.h
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#ifndef __agenda_cpp__amigo__
#define __agenda_cpp__amigo__

#include "pessoa.h"
#include <string>

using namespace std;

class Amigo : public Pessoa
{
private:
    string apelido;
    string email;
public:
    Amigo();
    Amigo(string apelido, string email);
    string getApelido();
    void setApelido(string apelido);
    string getEmail();
    void setEmail(string email);
};

#endif /* defined(__agenda_cpp__amigo__) */
