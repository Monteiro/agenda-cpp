//
//  main.cpp
//  agenda-cpp
//
//  Created by Franck Monteiro Santana on 02/04/15.
//  Copyright (c) 2015 Franck Monteiro Santana. All rights reserved.
//

#include <iostream>
#include "pessoa.cpp"
#include "amigo.cpp"
#include "contato.cpp"

using namespace std;


int main () {
    
    Pessoa x;
    Amigo y;
    Contato z;
    
    x.setNome ("Franck");
    x.setIdade ("18");
    x.setTelefone ("555-5555");
    
    y.setApelido ("Franckcisco");
    y.setEmail ("franck.monteiro@hotmail.com");
    
    z.setWpp ("9999-9999");
    z.setEndereco ("Brasilia");
    
    cout << "Nome: " << x.getNome() << endl;
    cout << "Idade: " << x.getIdade() << endl;
    cout << "Telefone: " << x.getTelefone() << endl;
    cout << "Apelido: " << y.getApelido() << endl;
    cout << "Email: " << y.getEmail() << endl;
    cout << "Celular: " << z.getWpp() << endl;
    cout << "Endereco: " << z.getEndereco() << endl;
    
    
    return 0;
}
